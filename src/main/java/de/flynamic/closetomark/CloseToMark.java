package de.flynamic.closetomark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import de.flynamic.userset.UserSet;
import scala.Tuple2;

public class CloseToMark {

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("UserSet").setMaster("local[*]");
		JavaSparkContext context = new JavaSparkContext(conf);

		JavaRDD<String> input = context.textFile("resources/last-fm-sample100000.tsv");
		
		// Split lines into columns
		JavaRDD<String[]> columns = input.map(line -> line.split("\\t"));
		
		// Map artists to usernames
		JavaPairRDD<String, String> artists = columns.mapToPair(cols -> {
			String username = cols[0];
			String artist = cols[3];
			return new Tuple2<String, String>(artist, username);
		});
		
		// Calculate user sets for each artist
		JavaPairRDD<String, UserSet> userSets = artists.combineByKey(username -> new UserSet(username),
				(userSet, username) -> userSet.add(username), (userSet1, userSet2) -> userSet1.add(userSet2));
		
		// Get Mark's userset
		Tuple2<String, UserSet> mark = userSets.filter(tuple -> tuple._1.equals("Mark Knopfler")).first();
		
		// Calculate distances to Mark
		JavaPairRDD<String, Double> distances = userSets
				.mapToPair(tuple -> new Tuple2<String, Double>(tuple._1, tuple._2.distanceTo(mark._2)));
		
		// Filter out distances that are bigger than or equal to 0.85
		JavaPairRDD<String, Double> closeToMark = distances.filter(tuple -> tuple._2 < 0.85);
		
		// Save text file
		closeToMark.saveAsTextFile("output/closetomark.txt");

		context.close();
	}

}
