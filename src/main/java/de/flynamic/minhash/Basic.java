package de.flynamic.minhash;

public class Basic {
	private static final int[] primeNumbers = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
			59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167,
			173 };

	public static long hash(int i, String s) {
		return ((primeNumbers[i] * s.hashCode() + primeNumbers[i + 20]) % 1009) % 1000;
	}

	public static void main(String[] args) {
		// Control
		if (Basic.hash(0, "Apple") == 769) {
			System.out.println("Correct.");
		}
	}
}
