package de.flynamic.minhash;

import java.io.Serializable;
import java.util.Arrays;
import java.util.stream.Stream;

public class MinHashAggregator implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected long[] minHashes = new long[20];
	
	public MinHashAggregator() {
		// Initialize minHashes
		Arrays.fill(minHashes, Long.MAX_VALUE);
	}

	public MinHashAggregator aggregate(String signature) {
		String[] minHashes = signature.split(",");
		for (int i = 0; i < minHashes.length; i++) {
			long minHash = Long.valueOf(minHashes[i]);
			this.minHashes[i] = Math.min(this.minHashes[i], minHash);
		}
		return this;
	}
	
	public MinHashAggregator aggregate(MinHashAggregator aggregator) {
		for (int i = 0; i < this.minHashes.length; i++) {
			this.minHashes[i] = Math.min(this.minHashes[i], aggregator.minHashes[i]);
		}
		return this;
	}
	
	public String toString() {
		Stream<String> minHashStrings = Arrays.stream(minHashes).<String>mapToObj(String::valueOf);
		String[] minHashStringArray = minHashStrings.toArray(String[]::new);
		return String.join(",", minHashStringArray);
	}

}
