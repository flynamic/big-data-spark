package de.flynamic.minhash;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import de.flynamic.userset.UserSet;
import scala.Tuple2;

public class UserMinHashAggregate {

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("UserMinHashAggregate").setMaster("local[*]");
		JavaSparkContext context = new JavaSparkContext(conf);
		
		JavaRDD<String> input = context.textFile("resources/last-fm-sample100000.tsv");
		
		JavaRDD<String[]> columns = input.map(line -> line.split("\\t"));
		
		JavaPairRDD<String, String> artists = columns.mapToPair(cols -> {
			String username = cols[0];
			String artist = cols[3];
			String minHashSignature = new UserSet(username).toMinHashSignature();
			return new Tuple2<String, String>(artist, minHashSignature);
		});
		
		// In Task (3), the UserSets had to be combined and thus communicated, before calculating the min hashes.
		// In Task (4) however, the min hashes are calculated in each node locally and then communicated. Unlike the UserSets,
		// they are capped to a length of 20.
		
		JavaPairRDD<String, MinHashAggregator> minHashes = artists.aggregateByKey(new MinHashAggregator(), 
				(agg, signature) -> agg.aggregate(signature), (agg1, agg2) -> agg1.aggregate(agg2));
		
		minHashes.saveAsTextFile("output/minhashes_aggregated.txt");
		context.close();
	}

}
