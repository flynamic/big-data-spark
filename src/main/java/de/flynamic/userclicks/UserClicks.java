package de.flynamic.userclicks;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class UserClicks {
	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("UserClicks").setMaster("local[*]");
		JavaSparkContext context = new JavaSparkContext(conf);
		
		JavaRDD<String> input = context.textFile("resources/last-fm-sample100000.tsv");
		JavaRDD<String[]> columns = input.map(line -> line.split("\\t"));
		JavaPairRDD<String, Integer> artistOne = columns.mapToPair(cols -> {
			return new Tuple2<String, Integer>(cols[3], new Integer(1));
		});
		JavaPairRDD<String, Integer> artistCount = artistOne.reduceByKey((accum, n) -> accum + n);
		
		artistCount.saveAsTextFile("output/userclicks.txt");
		context.close();
	}
}
