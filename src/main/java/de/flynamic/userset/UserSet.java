package de.flynamic.userset;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;

import de.flynamic.minhash.Basic;

public class UserSet implements Serializable {
	private static final long serialVersionUID = 1L;
	protected HashSet<String> set;

	public UserSet() {
		this.set = new HashSet<String>();
	}

	public UserSet(String... usernames) {
		this();
		this.set.addAll(Arrays.asList(usernames));
	}

	public UserSet add(String username) {
		this.set.add(username);
		return this;
	}

	public UserSet add(UserSet set) {
		this.set.addAll(set.set);
		return this;
	}

	public double distanceTo(UserSet other) {
		HashSet<String> intersectionSet = new HashSet<String>(this.set);
		intersectionSet.retainAll(other.set);
		int intersection = intersectionSet.size();
		HashSet<String> unionSet = new HashSet<String>(this.set);
		unionSet.addAll(other.set);
		int union = unionSet.size();
		return 1d - intersection / (double) union;
	}

	public String toMinHashSignature() {
		String[] minHashes = new String[20];
		for (int i = 0; i < 20; i++) {
			long minHash = Long.MAX_VALUE;
			for (String user : this.set) {
				long hash = Basic.hash(i, user);
				minHash = Math.min(minHash, hash);
			}
			minHashes[i] = String.valueOf(minHash);
		}
		return String.join(",", minHashes);
	}

	public String toString() {
		return this.set.toString();
	}
}
