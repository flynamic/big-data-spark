package de.flynamic.userset;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class UserSetMain {

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("UserSet").setMaster("local[*]");
		JavaSparkContext context = new JavaSparkContext(conf);
		
		JavaRDD<String> input = context.textFile("resources/last-fm-sample100000.tsv");
		JavaRDD<String[]> columns = input.map(line -> line.split("\\t"));
		JavaPairRDD<String, String> artists = columns.mapToPair(cols -> {
			String username = cols[0];
			String artist = cols[3];
			return new Tuple2<String, String>(artist, username);
		});
		JavaPairRDD<String, UserSet> userSets = artists.combineByKey(username -> new UserSet(username), 
				(userSet, username) -> userSet.add(username), (userSet1, userSet2) -> userSet1.add(userSet2));
		userSets.saveAsTextFile("output/usersets.txt");
		context.close();
	}

}
